# yammago project

Steps to run locally:

1. Clone the project
1. If you don't already have it, download the SDK for Google App Engine: https://cloud.google.com/appengine/docs/standard/python/download
1. From the command line, run `pip install -t lib -r requirements.txt`
1. OPTIONAL: start a local environment (Seth found this solved a few of his local deployment issues)
    1. If you don't have virtualenv, install it using `sudo pip install virtualenv`
    1. `virtualenv env`
    1. `source env/bin/activate`
    1. When you are done running the application, just run `deactivate`
1. If you are also running yammago-api on your localhost, you will need to update the API_ENDPOINT in main.py to point to localhost. Otherwise, you can leave it pointed at the appspot deployment of yammago-api: `API_ENDPOINT = "http://yammago-api.appspot.com/api/v1/"`
1. To start the application, run `dev_appserver.py app.yaml` from the root directory of the project (if you are running this AND yammago-api on your localhost, you will need to specify ports with the `--port` and `--admin_port` options. For example: `dev_appserver.py --port=8081 --admin-port=8001 app.yaml`)