from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField


class DescriptionForm(Form):
    body = TextAreaField('Description', [validators.Length(min=4, max=200)])



