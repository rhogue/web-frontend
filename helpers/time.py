import datetime
from dateutil import parser 

def fill_time(latest_event_list):
    for event in latest_event_list:
        dt_info = {}
        dt = parser.parse(event['start_at'])
        dt_info["day"] = dt.day
        dt_info["month"] = dt.strftime("%b")
        dt_info["year"] = dt.year 
        dt_info["time"] = dt.strftime("%-I %p")
        dt_info["dayname"] = dt.strftime("%a")
        event.setdefault('dt_info', dt_info)

    return latest_event_list

def fill_event_time(event):
    dt_info = {}
    dt = parser.parse(event['start_at'])
    dt_info["day"] = dt.day
    dt_info["month"] = dt.strftime("%b")
    dt_info["year"] = dt.year 
    dt_info["time"] = dt.strftime("%-I %p")
    dt_info["dayname"] = dt.strftime("%a")
    event.setdefault('dt_info', dt_info)
    return event


