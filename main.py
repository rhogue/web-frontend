import logging
import datetime
import requests
import urllib
import requests_toolbelt.adapters.appengine
from google.appengine.api import users
from flask import Flask, abort, render_template, request, jsonify, make_response, redirect, url_for
from functools import wraps
import helpers.time as time
import helpers.forms as forms
app = Flask(__name__)

# Use the App Engine Requests adapter. This makes sure that Requests uses
# URLFetch.
requests_toolbelt.adapters.appengine.monkeypatch()

API_ENDPOINT = "http://yammago-api.appspot.com/api/v1/"

def get_user(user_id):
    r = requests.get(API_ENDPOINT + 'users/' + str(user_id))
    if 'error' in r.json():
        return {'is_authenticated':False}
    user = r.json()['user']
    user ['is_authenticated'] = (users.get_current_user() != None)
    return user

def current_user():
    if (users.get_current_user() != None):
        return get_user(users.get_current_user().user_id())
    else:
        return {'is_authenticated':False}
#Set's the login to Google Users Service
def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not users.get_current_user():
            return redirect(users.create_login_url(request.url))
        else:
            user = users.get_current_user()
            return func(*args, **kwargs)
    return decorated_view

@app.route('/about')
def about():
    user_is_authenticated = (users.get_current_user() != None)
    return render_template('about.html', user=current_user())

@app.route('/')
@app.route('/events/')
def list_events():
    r = requests.get(API_ENDPOINT + 'events')
    r_content = r.json()
    latest_event_list = r_content['events']
    # making date/time extractable and printable in event listing
    latest_event_list = time.fill_time(latest_event_list)
    return render_template('events/index.html', latest_event_list=latest_event_list, user=current_user())

@app.route('/events/postal_code/<string:postal_code>')
def list_events_by_postal_code(postal_code):
    r = requests.get(API_ENDPOINT + 'events/postal_code/' + postal_code)
    r_content = r.json()
    latest_event_list = r_content['events']
    # making date/time extractable and printable in event listing
    latest_event_list = time.fill_time(latest_event_list)
    return render_template('events/index.html', latest_event_list=latest_event_list, user=current_user())


@app.route('/events/<int:event_id>', methods=['GET'])
def get_event(event_id):
    r = requests.get(API_ENDPOINT + 'events/' + str(event_id))
    r_content = r.json()
    user_is_authenticated = (users.get_current_user() != None)
    # check for error and show error page if not found
    desc_form = forms.DescriptionForm(body=r_content['event']['description'])
    going = [p for p in r_content['event']['participants'] if p['status'] == "Going"]
    interested = [p for p in r_content['event']['participants'] if p['status'] == "Interested"]
    going_interested="Not Going"
    if users.get_current_user():
        current_user_id = users.get_current_user().user_id()
        if any(p['user']['id'] == current_user_id for p in interested):
            going_interested="Interested"
        elif any(p['user']['id'] == current_user_id for p in going):
            going_interested="Going"
    user = current_user()
    user['going_interested']= going_interested
    event = time.fill_event_time(r_content['event'])
    return render_template('events/detail.html', event=event, going=going, interested=interested, user=user, desc_form=desc_form)

@app.route('/events/new', methods=['POST'])
@login_required
def create_new_event():
    newEvent={}
    for key, value in request.form.iteritems():
        newEvent[key] = value
    newEvent['owner'] = users.get_current_user().user_id()
    r = requests.post(API_ENDPOINT + 'events', json = newEvent)
    # if something went wrong w save (check status code of r), show message to user
    return redirect('/events')

@app.route('/comments/new', methods=['POST'])
@login_required
def create_new_comment():
    newComment={
        'user_id' : users.get_current_user().user_id(),
        'handle' : current_user()['handle'],
        'comment' : request.form['comment'],
        'event_id' : request.referrer.split('/')[-1]
    }
    print newComment
    r = requests.post(API_ENDPOINT + 'comments', json = newComment)
    # if something went wrong w save (check status code of r), show message to user
    return redirect(request.referrer)

@app.route('/event_participants/new', methods=['POST'])
@login_required
def create_new_event_participant():
    newEP={
        'user_id' : users.get_current_user().user_id(),
        'status' : request.json['status'],
        'event_id' : request.referrer.split('/')[-1]
    }
    r = requests.post(API_ENDPOINT + 'event_participants', json = newEP)
    # if something went wrong w save (check status code of r), show message to user
    return jsonify(r.json())

@app.route('/events/<int:event_id>/description', methods=['POST'])
@login_required
def description(event_id):
    newDesc={
        'description' : request.form['body']
    }
    r = requests.put(API_ENDPOINT + 'events/' + str(event_id), json = newDesc)
    return redirect(request.referrer)

@app.route('/login', methods=['GET'])
def login():
    urlsafe_rp = urllib.quote(request.referrer, safe='')
    return redirect(users.create_login_url('/post_login/' + urlsafe_rp))

@app.route('/post_login/<path:referrer>', methods=['GET'])
def post_login(referrer):
    current_user = users.get_current_user()
    user = {
        'user_id' : current_user.user_id(),
        'handle' : current_user.nickname()
    }
    r = requests.post(API_ENDPOINT + 'users', json = user)
    return redirect(urllib.unquote(referrer))

@app.route('/user/update/', methods=['POST'])
@login_required
def update_user():
    #set avatar
    user = {
        'user_id' : users.get_current_user().user_id()
    }
    if 'handle' in request.form and request.form['handle']:
        user['handle'] = request.form['handle']
    if 'firstname' in request.form and request.form['firstname']:
        user['firstname'] = request.form['firstname']
    if 'lastname' in request.form and request.form['lastname']:
        user['lastname'] = request.form['lastname']
    r = requests.put(API_ENDPOINT + 'users', json = user)
    return redirect(url_for('view_current_user'))

@app.route('/user', methods=['GET'])
@login_required
def view_current_user():
    user_id = users.get_current_user().user_id()
    user = get_user(user_id)
    return render_template('user/index.html', user=user, can_edit=True)

@app.route('/user/events', methods=['GET'])
@login_required
def list_user_events():
    user_id = users.get_current_user().user_id()
    r = requests.get(API_ENDPOINT + 'user_events/' + str(user_id))
    events = r.json()
    events['owner_of'] = time.fill_time(events['owner_of'])
    events['interested_in'] = time.fill_time(events['interested_in'])
    events['going_to'] = time.fill_time(events['going_to'])
    return render_template('user/events.html', user=current_user(), events=events)

@app.route('/user/<int:user_id>', methods=['GET'])
@login_required
def view_user(user_id):
    user = get_user(user_id)
    is_current_user = users.get_current_user().user_id() == str(user_id)
    return render_template('user/index.html', user=user, can_edit=is_current_user)

@app.route('/user/edit', methods=['GET'])
def edit_user():
    user_id = users.get_current_user().user_id()
    user = get_user(user_id)
    return render_template('user/edit.html', user=user)

@app.route('/logout', methods=['GET'])
def logout():
    return redirect(users.create_logout_url(request.referrer))

#MAKE THESE ERRORS PRETTY

@app.errorhandler(404)
def not_found(error):
    logging.exception('An error occurred during a request.')
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return make_response(jsonify({'error': 'An internal error occurred.'}), 500)
