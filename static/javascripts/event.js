function initMap() {
  var loc = document.getElementById('location').innerHTML;
  var geocode_url='https://maps.googleapis.com/maps/api/geocode/json?address='+loc.split(' ').join('+')+'&key=AIzaSyCnyywZ2ue0dEvB7WjNhGf4rA3KDnycc7E';
  $.get(
    geocode_url,
    function(data) {
      console.log(data);
       var loc_latlong = (data['results'][0]['geometry']['location']);
        var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: loc_latlong
      });
      var marker = new google.maps.Marker({
        position: loc_latlong,
        map: map,
        draggable:true,
        title:"Drag me!"
      });
    }
  );
}

// EDIT DESCRIPTION

$("#desc_button").on("click", function () {
  $("#description").html($("#editform").html());
});

// ADD TO CALENDAR

var CLIENT_ID = '354082150635-edq45d2psnkidup4ufmmfm4e87pr45pg.apps.googleusercontent.com';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
var SCOPES = "https://www.googleapis.com/auth/calendar";

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}
/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    clientId: CLIENT_ID,
    discoveryDocs: DISCOVERY_DOCS,
    scope: SCOPES
  });
}

function addEventToGoogleCal(){
  if(!gapi.auth2.getAuthInstance().isSignedIn.get()){
    gapi.auth2.getAuthInstance().signIn().then(function () {
      addEventToGCal();
    });
  } else {
    addEventToGCal();
  }
}

function ISODateString(d){
  function pad(n){return n<10 ? '0'+n : n}
  return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth()+1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z';
}
