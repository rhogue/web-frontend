// Search by postal code
$('#postal_code_search_submit').on("click", function () {
  let postal_code = $('#postal_code_search').val();
  window.location.href = '/events/postal_code/' + postal_code;
});
